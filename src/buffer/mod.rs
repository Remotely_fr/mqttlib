/* The size of a ring buffer in bytes */
pub const RING_SIZE: usize = 131072;

/* Structure representing a ring buffer. */
pub struct Ring
{
	/* The position of the reading cursor */
	read_cursor: usize,
	/* The position of the writing cursor */
	write_cursor: usize,
	/* The data container */
	data: [u8; RING_SIZE],
}

impl Ring
{
	/* Creates a new ring buffer with the given `size` in bytes */
	pub fn new() -> Self {
		Self {
			read_cursor: 0,
			write_cursor: 0,
			data: [0; RING_SIZE],
		}
	}

	/* Returns the number of used bytes in the buffer */
	pub fn used_size(&self) -> usize {
		// TODO
		0
	}

	/* Returns the number of free bytes in the buffer */
	pub fn remaining_size(&self) -> usize {
		RING_SIZE - self.used_size()
	}

	/* Pushes data onto the ring buffer */
	pub fn push(buff: &[u8]) {
		// TODO
	}

	/* Peeks `size` bytes of data from the ring buffer */
	pub fn peek(size: usize, buff: &mut [u8]) {
		// TODO
	}

	/* Pops `size` bytes of data off the ring buffer */
	pub fn pop(size: usize, buff: &mut [u8]) {
		// TODO
	}
}
