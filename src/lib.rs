pub mod buffer;

/* Structure representing a MQTT context */
pub struct Context<A, B, C>
{
	/* Tells whether the context is a broker */
	broker: bool,

	/* The function to call to check if the socket is ready for reading */
	ready_hook: A,
	/* The function to call to read data from socket */
	read_hook: B,
	/* The function tot call to write data to socket */
	write_hook: C,

	/* The ring buffer to store incoming data */
	buffer: buffer::Ring,
}

impl<A, B, C> Context<A, B, C>
	where A: Fn() -> bool,
		B: Fn(&mut [u8], usize) -> isize,
		C: Fn(&[u8], usize) -> isize,
{
	/* Creates a new MQTT context */
	pub fn new(broker: bool, ready_hook: A, read_hook: B, write_hook: C) -> Self {
		Self {
			broker: broker,
			ready_hook: ready_hook,
			read_hook: read_hook,
			write_hook: write_hook,
			buffer: buffer::Ring::new(),
		}
	}

	// TODO Packet sending function

	/* Fills the ring buffer with incoming data */
	fn fill_buffer(&mut self) {
		// TODO Fill ring buffer from incoming data
	}

	/* Tick the context, triggering data reading and packet parsing */
	pub fn tick(&mut self) {
		if !(self.ready_hook)() {
			return;
		}
		self.fill_buffer();
		// TODO Parse packet
	}
}
